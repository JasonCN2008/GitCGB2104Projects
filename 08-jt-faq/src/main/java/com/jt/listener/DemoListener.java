package com.jt.listener;

import org.springframework.stereotype.Component;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 这里的监听器用于监听服务的启动和销毁
 */

public class DemoListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("服务启动了");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("服务销毁了");
    }
}
