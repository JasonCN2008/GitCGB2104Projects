package com.jt.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

     @GetMapping("/mvc")
     public String doHandle(){
         return "hello spring web mvc";
     }

}
