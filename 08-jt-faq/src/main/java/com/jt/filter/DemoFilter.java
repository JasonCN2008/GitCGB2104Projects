package com.jt.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * javaee规范中的过滤器,对请求和响应数据进行过滤
 * 1)统一数据的编码
 * 2)统一数据格式校验 (今日头条的灵犬系统)
 * 3)统一身份认证
 */
public class DemoFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        System.out.println("==doFilter==");
        servletRequest.setCharacterEncoding("UTF-8");
        String id= servletRequest.getParameter("id");
        System.out.println("id="+id);
        filterChain.doFilter(servletRequest,servletResponse);
    }
}
