package com.jt.config;

import com.jt.filter.DemoFilter;
import com.jt.listener.DemoListener;
import com.jt.servlet.DemoServlet;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 在这里配置javaee规范中的三大组件
 */
@Configuration
public class ComponentConfig {

    //注册servlet
    @Bean
    public ServletRegistrationBean servletRegistrationBean(){
        ServletRegistrationBean bean=new ServletRegistrationBean();
        bean.setServlet(new DemoServlet());
        bean.addUrlMappings("/hello");
        return bean;
    }

    //注册过滤器
    @Bean
    public FilterRegistrationBean filterRegistrationBean(){
        FilterRegistrationBean bean=
                new FilterRegistrationBean(new DemoFilter());
        bean.addUrlPatterns("/hello");//对哪个请求进行处理
        return bean;
    }
    //注册监听器
    @Bean
    public ServletListenerRegistrationBean servletListenerRegistrationBean(){
        ServletListenerRegistrationBean bean=
                new ServletListenerRegistrationBean(new DemoListener());
        return bean;
    }
}
