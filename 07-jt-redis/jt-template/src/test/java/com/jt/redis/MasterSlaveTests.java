package com.jt.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class MasterSlaveTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testMasterReadWrite(){//6379
        ValueOperations valueOperations = redisTemplate.opsForValue();
        valueOperations.set("role","master6379");
        Object result=valueOperations.get("role");
        System.out.println(result);
    }
    @Test
    void testSlaveRead(){//6380
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Object result=valueOperations.get("role");
        System.out.println(result);
    }

}
