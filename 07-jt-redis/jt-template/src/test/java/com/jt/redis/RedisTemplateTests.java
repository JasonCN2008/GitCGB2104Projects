package com.jt.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
public class RedisTemplateTests {
    //ResdisTemplate主要用于操作redis中的复杂对象
    //底层序列化方式为JDK方式的序列化:JdkSerializationRedisSerializer
    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void testConnection(){
        String result=
        redisTemplate.getConnectionFactory().getConnection().ping();
        System.out.println(result);
    }
    //测试String类型数据
    @Test
    void testStringOper(){
        ValueOperations<String,Integer> valueOperations = redisTemplate.opsForValue();
        valueOperations.set("num",100);
        Integer num=valueOperations.get("num");
        System.out.println("num="+num);
    }
    //测试list类型数据
    @Test
    void testListOper(){//list类型数据有序存储(按添加顺序),元素允许重复
        ListOperations listOperations = redisTemplate.opsForList();
        listOperations.leftPush("lst1",100);
        listOperations.leftPushAll("lst1",200,300,400);
        List lst1 = listOperations.range("lst1", 0, -1);
        System.out.println(lst1);
        Object element = listOperations.rightPop("lst1");//FIFO 先进先出
        System.out.println(element);
    }
    //测试set类型数据
    @Test
    void testSetOper(){//Set类型数据无序,元素不允许重复
        SetOperations setOperations = redisTemplate.opsForSet();
        setOperations.add("set",100,200,300,300);
        Set set = setOperations.members("set");
        System.out.println(set);
        Long count = setOperations.remove("set", 100,200,300);
        System.out.println("count="+count);//移除的元素个数
    }
    //测试hash类型
    @Test
    void testHashOper(){
        //获取操作hash类型数据的对象
        HashOperations hashOperations = redisTemplate.opsForHash();
        //存数据
        hashOperations.put("blog","id",101);
        hashOperations.put("blog","title","redis应用类型");
        //取指定对象对应的字段的值
        Object id = hashOperations.get("blog", "id");
        System.out.println("id="+id);
        //取指定对象所有字段的值
        Map blog = hashOperations.entries("blog");
        System.out.println(blog);
    }
    //清redis数据库数据
    @Test
    void testFlushAll(){
         redisTemplate.execute(new RedisCallback() {
             @Override
             public Object doInRedis(RedisConnection redisConnection) throws DataAccessException {
                 //redisConnection.flushDb();
                 redisConnection.flushAll();
                 return "flush ok";
             }
         });
    }

}
