package com.jt.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class RedisClusterTests {

     @Autowired
     private RedisTemplate redisTemplate;//JDK
     @Test
     void testRedisCluster(){
         ValueOperations valueOperations = redisTemplate.opsForValue();
         valueOperations.set("key1","100");
         Object key1 = valueOperations.get("key1");
         System.out.println(key1);
     }

}
