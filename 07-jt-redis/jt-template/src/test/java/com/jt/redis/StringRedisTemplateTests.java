package com.jt.redis;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class StringRedisTemplateTests {

    //StringRedisTemplate 主要作用用于操作字符串类型数据
    //底层序列化方式为 StringRedisSerializer
    @Autowired
    private StringRedisTemplate redisTemplate;
    //测试redis的连接
    @Test
    void testConnection(){
        RedisConnection connection =
                redisTemplate.getConnectionFactory()
                        .getConnection();
        String ping = connection.ping();
        System.out.println(ping);
    }
    //基于StringRedisTemplate做字符串的操作测试
    @Test
    void testStringOper(){
        //1.获取字符串操作对象
        ValueOperations<String, String> valueOperations =
                redisTemplate.opsForValue();
        //2.向redis存储数据
        valueOperations.set("id","0");
        //3.更新redis中的数据
        valueOperations.increment("id");
        //4.获取redis中数据
        String id=valueOperations.get("id");
        System.out.println(id);
    }
    //....

}
