package com.jt.controller;

import com.jt.annotation.Logable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CacheDemoController02 {
    //private static final Logger log= LoggerFactory.getLogger(CacheDemoController02.class);
    /**
     * 底层就是AOP,由@Cacheable注解描述的方法为一个切入点方法,
     * 这个方法在执行时,会通过代理对象进行缓存功能增强,@Cacheable注解
     * 的含义是告诉底层系统,将方法的方法的返回值存储到Cache中,
     */
    @Logable
    @Cacheable(value="demo2")
    @GetMapping("/demo2/hello")
    public  String doSayHello(){
        //log.info("start:"+System.currentTimeMillis());
        System.out.println("==Get Hello From MySql===");
        String result="hello redis";//假设来自数据库
        //log.info("end:"+System.currentTimeMillis());
        return result;
    }
}

