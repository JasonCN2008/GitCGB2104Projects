package com.jt.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Order(Integer.MIN_VALUE)//最高优先级
@Aspect//这个注解描述类为一个切面对象
@Component
public class LogAspect {
     private static final Logger log= LoggerFactory.getLogger(LogAspect.class);
     /**定义一个切入点:所有由@Logable注解描述的方法都为切入点方法,这个方法执行之前会被拦截,然后
      * 基于代理方式进行功能扩展*/
     @Pointcut("@annotation(com.jt.annotation.Logable)")
     public void doLog(){}//这里不需要写任何内容,只负责承载@Pointcut注解

    /**
     * 定义一个通知方法,在这个方法内部做扩展逻辑业务实现
     * @Around注解描述的方法内部可以基于条件手动调用目标方法
     * @param joinPoint 连接点对象,此对象封装了你要执行的目标方法信息
     * @return 表示目标方法的执行结果
     * @throws Throwable
     */
     @Around("doLog()")
     public Object around(ProceedingJoinPoint joinPoint)throws Throwable{
         Signature signature = joinPoint.getSignature();//获取方法签名
         log.info("{} start:{}",signature.getName(),System.currentTimeMillis());
         Object result=joinPoint.proceed();//最终会执行目标方法
         log.info("{} end:{}",signature.getName(),System.currentTimeMillis());
         return result;
     }
}
