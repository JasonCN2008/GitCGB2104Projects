package com.jt.demos;

import redis.clients.jedis.Jedis;

/**
 * 需求:生成一个分布递增的id
 * 多张表基于这个方法中生成的id作为主键id值(分布式环境不会采用数据库
 * 表中自带的自增策略-auto_increment)
 */
public class IdGeneratorDemo01 {
    public static Long getId(){
          Jedis jedis=new Jedis("192.168.126.130",6379);
          jedis.auth("123456");
          Long id = jedis.incr("id");
          jedis.close();
          return id;
    }
    public static void main(String[] args) {
         for(int i=0;i<10;i++) {
              new Thread(){
                  @Override
                  public void run() {
                      String tName=Thread.currentThread().getName();
                      System.out.println(tName+"->"+
                              IdGeneratorDemo01.getId());
                  }
              }.start();
          }

    }
}
