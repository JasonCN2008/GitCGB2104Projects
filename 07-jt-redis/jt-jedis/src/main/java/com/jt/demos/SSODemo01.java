package com.jt.demos;

import redis.clients.jedis.Jedis;

import java.util.*;

/**
 * 基于redis存储用户登录信息的单点登录系统
 */
public class SSODemo01 {
    //校验session的有效性
    public static boolean isValidSession(String token){
        if(token==null||"".equals(token)){
            return false;
        }
        //1.建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");
        //2.从redis获取会话信息
        //2.1用户token对应的用户信息,将来可以是一个json格式的字符串
        String user=jedis.hget("session",token);//session这里表示会话对象
        System.out.println("user="+user);
        if(user==null||"".equals(user)){
            System.out.println("还没有登录");
            jedis.close();
            return false;
        }
        //3.判定是否登录超时
        String dateStr=jedis.hget("session",token+":expired");
        Date expired=new Date(Long.valueOf(dateStr));
        if(dateStr==null||"".equals(dateStr)||expired.before(new Date())) {
            System.out.println("登录超时");
            jedis.close();
            return false;
        }
        return true;
    };

    //执行登录操作
    public static String login(String username,String password){
     System.out.println("基于"+username+"/"+password+"进行登录");
        //1.创建token(一般可以用一个随机的字符串)
       String token= UUID.randomUUID().toString()
               .replace("-","");
       System.out.println(token);
       //2.创建有效登录时长
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.MINUTE,30);
       //3.存储会话信息
        Map<String,String> map=new HashMap<>();
        map.put(token,username);//username这里可以是包含更多用户信息的json字符串
        map.put(token+":expired",String.valueOf(calendar.getTimeInMillis()));
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");
        jedis.hset("session",map);
        jedis.close();
        return token;
    }
    public static void main(String[] args) {
        System.out.println("===访问系统资源===");
        //第一访问:访问redis,检查是否有有效的会话信息
        String token=null;
        boolean flag=isValidSession(token);
        System.out.println(flag?"第一次:已登录":"第一次:未登录");
        //执行登录
        if(!flag){
            System.out.print("执行登录:");
            token=login("jack","123456");
            //登录成功的这个token将来响应到客户端
        }
       //第二次访问:
        flag=isValidSession(token);
        System.out.println(flag?"第二次:已登录":"第二次:未登录");
        //第三次访问:
        flag=isValidSession(token);
        System.out.println(flag?"第三次:已登录":"第三次:未登录");
    }
}
