package com.jt.demos;

import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * 投票系统演示:模拟基于某个活动的投票程序
 * 业务说明:一个用户只能投票一次(不允许重复)
 * 数据结构设计:基于redis的set类型进行数据存储
 */
public class VoteDemo01 {
    private static Jedis jedis=
            new Jedis("192.168.126.130",6379);
    static{
        jedis.auth("123456");
    }
    //进行投票(key为活动id,value为userId)
    static void vote(String activityId,String userId){
         jedis.sadd(activityId,userId);
    }
    //查看投票次数
    static Long getVoteCount(String activityId){
        Long count = jedis.scard(activityId);
        return count;
    }
    //查看这个活动被哪写用户投过票
    static Set<String> getVoteUsers(String activityId){
        Set<String> smembers = jedis.smembers(activityId);
        return smembers;
    }
    //检查这个用户是否已对这个活动投过票
    static Boolean checkVote(String activityId,String userId){
        Boolean flag = jedis.sismember(activityId, userId);
        return flag;
    }

    public static void main(String[] args) {
        //0.初始化
        String activityId="10001";
        String user1="201";
        String user2="202";
        String user3="203";
        //1.投票
        vote(activityId,user1);
        vote(activityId,user2);
        vote(activityId,user3);
        //2.获取投票次数
        Long voteCount = getVoteCount(activityId);
        System.out.println("投票次数:"+voteCount);
        //3.输出哪些人投过票
        Set<String> users=getVoteUsers(activityId);
        System.out.println(users);
        //4.检查用户是否投过票
        boolean flag=checkVote(activityId,user1);
        System.out.println(user1+":"+(flag?"已投过票":"还没投票"));
    }

}
