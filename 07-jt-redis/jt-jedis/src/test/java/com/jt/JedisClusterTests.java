package com.jt;
import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

public class JedisClusterTests {

    @Test
    public void testRedisCluster(){
          //1.构建集群对象
        Set<HostAndPort> nodes=new HashSet<>();
        nodes.add(new HostAndPort("192.168.126.130",8010));
        nodes.add(new HostAndPort("192.168.126.130",8011));
        nodes.add(new HostAndPort("192.168.126.130",8012));
        nodes.add(new HostAndPort("192.168.126.130",8013));
        nodes.add(new HostAndPort("192.168.126.130",8014));
        nodes.add(new HostAndPort("192.168.126.130",8015));
        JedisCluster jedisCluster=new JedisCluster(nodes);
        jedisCluster.set("redis","6.2.5");
        String redis = jedisCluster.get("redis");
        System.out.println(redis);
        jedisCluster.close();
    }
}
