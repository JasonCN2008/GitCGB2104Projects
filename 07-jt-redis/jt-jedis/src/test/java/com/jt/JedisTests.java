package com.jt;

import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class JedisTests {

    @Test
    public void testStringOper() throws InterruptedException {
        //建立链接(与redis建立链接)
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        String result=jedis.ping();
        System.out.println(result);

        //存储数据(key/value)
        jedis.set("count","1");
        jedis.set("id","10001");
        jedis.set("content","aaaaaaaadfas");
        //更新数据
        jedis.expire("id",1);//设置key的有效时长
        jedis.incr("count");//对key的值进行自增操作
        //获取数据
        String count = jedis.get("count");
        //TimeUnit是Java中枚举类型,SECONDS为枚举类型的实例,sleep底层会调用Thread.sleep()方法
        //TimeUnit.SECONDS.sleep(1);//休眠一秒
        Thread.sleep(1000);
        String id=jedis.get("id");
        Long num=jedis.strlen("content");
        System.out.println("cart.count="+count);
        System.out.println("id="+id);
        System.out.println("num="+num);
        //释放资源
        jedis.close();
    }
    //将对象转换为json字符串,然后写入redis
    @Test
    public void testStrJsonOper(){
        //建立连接
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        String result=jedis.ping();
        System.out.println(result);
        //假如基于字符串类型存储对象如何实现?(例如一个博客对象)
        Map<String,Object> map=new HashMap<>();
        map.put("id",101);
        map.put("title","分布式缓存");
        map.put("content","redis database");
        //将map对象转换为json格式字符串
        //这里借助Google公司的gson将对象转换为json字符串
        Gson gson=new Gson();
        String jsonMap=gson.toJson(map);
        jedis.set("blog",jsonMap);
        jsonMap=jedis.get("blog");
        System.out.println("jsonMap="+jsonMap);
        //将字符串转换为map进行操作
        map=gson.fromJson(jsonMap,Map.class);
        System.out.println(map);
        jedis.close();
    }
    //作业,尝试实现哈希,列表,集合类型的操作
    //hash类型一般用于存储对象(一个对象有很多属性和值)
    @Test
    public void testHashOper01(){
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        Map<String,String> map=new HashMap<>();
        map.put("id","101");
        map.put("title","分布式缓存");
        map.put("content","redis database");
        jedis.hset("article",map);
        map=jedis.hgetAll("article");
        System.out.println(map);
        jedis.close();
    }
    @Test
    public void testHashOper02(){
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        jedis.hset("map","id","101");
        jedis.hset("map","title","title-aaa");
        String id=jedis.hget("map","id");
        String title=jedis.hget("map","title");
        System.out.println(id+"/"+title);
        jedis.close();
    }
    //基于list实现一个先进先出的非阻塞队列:FIFO
    @Test
    public void testListOper01(){
        Jedis jedis=new Jedis("192.168.126.130",6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        jedis.lpush("lst1","A","B","C");
        String a = jedis.rpop("lst1");
        String b = jedis.rpop("lst1");
        String c = jedis.rpop("lst1");
        System.out.println(a+":"+b+":"+c);
        String d = jedis.rpop("lst1");//空了,还可以继续取
        System.out.println("d="+d);
//          List<String> list=
//                jedis.rpop("lst1",
//                        jedis.llen("lst1").intValue());
//        System.out.println("list="+list);
        jedis.close();
    }
    //list集合的阻塞式队列实现,当从集合取数据时,假如已空,则阻塞.
    @Test
    public void testListOper02() {
        Jedis jedis = new Jedis("192.168.126.130", 6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        jedis.lpush("lst1", "A", "B", "C");
        jedis.brpop(60,"lst1");//brpop为一个阻塞式方法
        jedis.brpop(60,"lst1");
        jedis.brpop(60,"lst1");
        System.out.println("list集合已经空了");
        jedis.brpop(60,"lst1");
        System.out.println("list集合被阻塞了60秒");
        jedis.close();
    }
    @Test
    public void testSetOper(){
        Jedis jedis = new Jedis("192.168.126.130", 6379);
        jedis.auth("123456");//假如设置了密码,需要密码认证
        jedis.sadd("set1","A","B","C","C");
        Set<String> set1 = jedis.smembers("set1");
        System.out.println(set1);
        jedis.close();
    }
}