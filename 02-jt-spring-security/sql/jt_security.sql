DROP DATABASE IF EXISTS `jt_security`;
CREATE DATABASE  `jt_security` DEFAULT CHARACTER SET utf8mb4;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

use `jt_security`;
CREATE TABLE `sys_menu` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
                            `name` varchar(50) NOT NULL COMMENT '权限名称',
                            `permission` varchar(200) DEFAULT NULL COMMENT '权限标识',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='权限表';
CREATE TABLE `sys_role` (
                            `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                            `role_name` varchar(50) NOT NULL COMMENT '角色名称',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='角色表';
CREATE TABLE `sys_role_menu` (
                                 `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
                                 `role_id` bigint(11) DEFAULT NULL COMMENT '角色ID',
                                 `menu_id` bigint(11) DEFAULT NULL COMMENT '权限ID',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8  COMMENT='角色与权限关系表';

CREATE TABLE `sys_user` (
                            `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                            `username` varchar(50) NOT NULL COMMENT '用户名',
                            `password` varchar(100) DEFAULT NULL COMMENT '密码',
                            `status` varchar(10) DEFAULT NULL COMMENT '状态 PROHIBIT：禁用   NORMAL：正常',
                            PRIMARY KEY (`id`) USING BTREE,
                            UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8  COMMENT='系统用户表';

CREATE TABLE `sys_user_role` (
                                 `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
                                 `user_id` bigint(11) DEFAULT NULL COMMENT '用户ID',
                                 `role_id` bigint(11) DEFAULT NULL COMMENT '角色ID',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8  COMMENT='用户与角色关系表';

INSERT INTO `sys_menu` VALUES (1, 'select users', 'sys:user:info');
INSERT INTO `sys_menu` VALUES (2, 'select menus', 'sys:menu:info');
INSERT INTO `sys_menu` VALUES (3, 'select roles', 'sys:role:info');
INSERT INTO `sys_role` VALUES (1, 'ADMIN');
INSERT INTO `sys_role` VALUES (2, 'USER');
INSERT INTO `sys_role_menu` VALUES (1, 1, 1);
INSERT INTO `sys_role_menu` VALUES (2, 1, 2);
INSERT INTO `sys_role_menu` VALUES (3, 1, 3);
INSERT INTO `sys_role_menu` VALUES (4, 2, 1);
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$5T851lZ7bc2U87zjt/9S6OkwmLW62tLeGLB2aCmq3XRZHA7OI7Dqa', 'NORMAL');
INSERT INTO `sys_user` VALUES (2, 'user', '$2a$10$szHoqQ64g66PymVJkip98.Fap21Csy8w.RD8v5Dhq08BMEZ9KaSmS', 'NORMAL');
INSERT INTO `sys_user_role` VALUES (1, 1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2, 2);
