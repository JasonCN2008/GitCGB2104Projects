package com.jt.sso.controller;

import com.jt.sso.util.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AuthController {

    @GetMapping("/auth/info")
    public Map<String,Object> getAuthentication(String token){
        //1.解析token
        Claims claims=JwtUtils.getClaimsFromToken(token);
        //2.判断token的有效期
       Boolean expired=claims.getExpiration().before(new Date());
        //3.获取用户名
        String username=(String)claims.get("username");
        //4.获取用户权限
        List<String> authorities=(List<String>)claims.get("authorities");
        //5.封装数据并返回
        Map<String,Object> map=new HashMap<>();
        map.put("expired",expired);
        map.put("username",username);
        map.put("authorities",authorities);
        return map;
    }
}
