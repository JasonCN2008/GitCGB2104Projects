package com.jt.res.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**远程认证服务调用接口
 * @FeignClient用于描述远程调用服务接口,系统启动时会为接口创建其实现类
 * name的值为你要调用的服务名
 * contextId为你将服务接口对象交给spring管理时,bean的名字,没有指定时,
 * 默认使用name的值
 * */

@FeignClient(name = "jt-sso-auth",contextId = "remoteAuthService")
public interface RemoteAuthService {
     //@GetMapping中的地址为jt-sso-auth服务中的一个地址
     @GetMapping("/auth/info")
     public Map<String,Object> getAuthentication(
             @RequestParam("token") String token);
}
