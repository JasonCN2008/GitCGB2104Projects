package com.jt.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

@Configuration
public class CorsFilterConfig {//Spring Cloud Gateway 底层用的Netty+WebFlux技术
    /**创建跨域过滤器的配置
     * 这个CorsWebFilter由WebFlux框架提供,做跨域涉设计时,
     * 我们只需要对允许跨域访问的资源进行放行就可以了**/
    @Bean
    public CorsWebFilter corsFilter(){
        UrlBasedCorsConfigurationSource source=
                new UrlBasedCorsConfigurationSource();
                //new UrlBasedCorsConfigurationSource(new PathPatternParser());
        CorsConfiguration corsConfiguration=new CorsConfiguration();
        //1.配置跨域
        //允许哪种请求头跨域,"*"代表所有
        corsConfiguration.addAllowedHeader("*");
        //允许哪种方法类型跨域 get post delete put
        corsConfiguration.addAllowedMethod("*");
        // 允许哪些请求源跨域
        corsConfiguration.addAllowedOrigin("*");
        // 是否携带cookie跨域
        corsConfiguration.setAllowCredentials(true);
        //允许跨域的路径
        source.registerCorsConfiguration("/**",corsConfiguration);

        return new CorsWebFilter(source);
    }
}
