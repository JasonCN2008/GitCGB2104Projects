package com.cy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@SpringBootApplication
public class ProviderApplication {
    //org.slf4j.Logger (Log4J,Logback都是基于此规范实现的日志库)
    //org.slf4j.LoggerFactory
    //getLogger(哪个类中输出的日志)
    private static final Logger log=
            LoggerFactory.getLogger(ProviderApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class,args);
    }

    //当配置中心的内容发生变化,重新发布时,用于刷新类中的配置信息
    @RefreshScope
    @RestController
    public class ProviderController{
        @Value("${server.port}")
        private String server;
        /**读取配置中心的日志级别*/
        @Value("${logging.level.com.cy:info}")//":"后面为默认值
        private String logLevel;

        @Value("${server.tomcat.threads.max:50}")
        private int maxThread;

        @Value("${page.pageSize:3}")
        private int pageSize;

        public ProviderController(){
            System.out.println("ProviderController()");
        }

        @GetMapping("/provider/pageSize")
        public String doGetPageSize(){
            return "page size is "+pageSize;
        }
        @GetMapping("/provider/maxThread")
        public String doGetMaxThread() throws InterruptedException {
            String tName=Thread.currentThread().getName();
            System.out.println("tName="+tName);
            //Thread.sleep(50000);
            return "max thread is "+maxThread;
        }

        @GetMapping("/provider/logLevel")
        public String doGetLogLevel(){
            //System.out.println("log.level="+logLevel);
            //日志优先级 trace<debug<info<warn<<error
            log.trace("==log.trace==");//跟踪
            log.debug("==log.debug==");//调试
            log.info("==log.info==");//常规信息
            log.warn("==log.warn==");//警告
            log.error("==log.error==");//错误信息
            return "log level is "+logLevel;
        }

        @GetMapping("/provider/echo/{msg}")
        public String doEcho(@PathVariable String msg) throws InterruptedException {//Echo为回显的含义
              //Thread.sleep(500000);//模拟耗时操作
            //通过如下语句默认服务调用过程中的异常
              //if(msg==null||msg.length()<3)
              //    throw new IllegalArgumentException("参数不合法");
              return server+" say: hello "+msg;
          }
    }
}
