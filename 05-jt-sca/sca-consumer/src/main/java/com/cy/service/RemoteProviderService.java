package com.cy.service;

import com.cy.service.factory.ProviderFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**基于@FeignClient定义远程服务调用接口,这个接口不需要我们自己写实现类,
 * 底层会在服务启动时帮我们创建实现(代理对象),在实现类内部进行远程服务调用
 * 其中:
 * 1)name属性的值为nacos中的服务名
 * 2)contextId为一个上下文id,一般就写当前接口名,用于区分服务调用,
 * 因为将来一个服务提供方会提供很多资源调用,假如服务消费方法基于同一
 * 个服务提供方写了很多服务调用接口,此时假如没有指定contextId,服务
 * 启动就会失败.例如:
 * @FeignClient(name="sca-provider")
 * public interface RemoteOtherService {
 *      @GetMapping("/doSomeThing")
 *      public String doSomeThing();
 * }
 * 3)fallBackFactory用于指定服务中断或异常时,应该返回给客户端的默认结果
 * */
@FeignClient(name = "sca-provider",
             contextId = "remoteProviderService",
             fallbackFactory = ProviderFallbackFactory.class)//sca-provider为nacos中的服务名
public interface RemoteProviderService {
    @GetMapping("/provider/echo/{msg}")
    public String echoMsg(@PathVariable String msg);
}
