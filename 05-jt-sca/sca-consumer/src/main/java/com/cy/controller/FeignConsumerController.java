package com.cy.controller;

import com.cy.service.RemoteProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 演示基于feign方式服务调用
 */
@RestController
public class FeignConsumerController {
    @Autowired
    private RemoteProviderService remoteProviderService;
    //http://localhost:8090/feign/echo/8090
    @GetMapping("/feign/echo/{msg}")
    public  String doFeignEcho(@PathVariable  String msg){
        return remoteProviderService.echoMsg(msg);
    }
}
